# Métodos de ordenamiento interno

El programa presente llena arreglos unidimensionales de manera aleatoria y en la cantidad ingresada por el usuario. Posteriormente los ordena con dos métodos de ordenamiento interno, Selection y Quicksort. Señala al final el tiempo que se demoró cada método en ordenar de manera creciente los arreglos y, si el usuario lo señala como tal, imprime los datos de los arreglos.

## Comenzando

Los métodos de ordenamiento presentados en el programa son Selection, método directo O(n²) que es eficiente pero a medida que n aumenta se hace más lento, y Quicksort, método logarítmico O(n * logn) que es considerado el más eficiente. Son métodos internos debido a que se guardan los datos en la memoria RAM y no una memoria externa como discos o cintas.

La esencia del programa radica en comparar el tiempo que demora cada método en ordenar crecientemente un arreglo con n cantidad de datos. Se le solicita al usuario el parámetro de entrada N para la cantidad de elementos dentro del arreglo y si desea ver el contenido del arreglo. Procede a ordenar cada uno por un método y, si el usuario señaló querer ver los elementos del arreglo, muestra su contenido junto con el tiempo que se demoró cada uno en ordenarlos. Es útil para determinar cual método es más eficiente y bajo qué condiciones.

## Prerrequisitos

### Sistema Operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

## Ejecutando las pruebas

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ programa.cpp Ordenar.cpp -o programa`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando:

`./programa`

Al momento que se ejecuta el programa pide inmediatamente la cantidad de elementos que tendrá dicho arreglo. Esta cantidad debe ser de carácter entero y positivo, un número entre el 0 y 1000000; si lo que ingresa no está dentro de estos parámetros entonces se volverá a solicitar que ingrese un número hasta que ingrese uno o realice una salida forzosa del programa.

Posterior al ingreso de la cantidad solicitada se le pregunta al usuario si desea ver los elementos contenidos en el arreglo. Para que la respuesta sea efectiva se debe ingresar la letra "s", el ingreso de cualquier otro caracter simbolizará como una respuesta negativa a la pregunta.

El programa crea un arreglo de dimensión n, solicitado al inicio, que rellena con números aleatorios entre el 1 y el 1000000. Se crean dos copias independientes del arreglo para poder hacer ambos órdenes sin influenciar al arreglo original. Primero ordena por el método de selección y luego por el método de quicksort. Si la respuesta a ver el contenido es positiva entonces se imprime el arreglo de la manera en la que fue creado, luego el tiempo que demoró cada método de ordenamiento y como quedaron ordenados los elementos con cada método. Si la respuesta a ver el contenido es negativa entonces solo se muestra los tiempos. Estos tiempos son en milisegundos (1 s * 10⁻³)

Una vez terminados de ordenar los arreglos en cada método el programa finaliza.

La salida forzosa del programa en cualquier parte de su ejecución es mediante `ctl+z`o `ctl+c`.

## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje c++: librería iostream, cstring, cstdlib, ctime, sstrem.

## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl
