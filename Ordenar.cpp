#include <iostream>
//#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "Ordenar.h"

// constructor
Ordenar::Ordenar(){}

// imprime los arreglos
void Ordenar::imprimir(int array[], int n, string metodo)
{
  cout << metodo << "      : "; // si es selection o quicksort
  // uno por uno
  for(int i=0; i<n; i++){
    cout << "arreglo["<< i << "]=" << array[i] << " ";
  }
  // salto de línea una vez terminado el arreglo
  cout << endl;
}

// imprime cuanto se demoró cierto método
void Ordenar::imprimir_tiempo(clock_t start, clock_t finish, string metodo)
{
  double secs;
  secs = ((finish - start) / (double)CLOCKS_PER_SEC) * 1000;
  cout << metodo << "         |" << secs << " milisegundos" << endl;
  //cout << " -----------------------------------------" << endl;
}

// método de orden selección O(n²)
void Ordenar::seleccion(int array[], int n)
{
  // empieza a correr el tiempo
  clock_t start = clock();
  // variables
  int i, menor, k, j;

  // se busca el elemento más pequeño
  for (i=0; i<=n-2; i++) {
    /*elemento contenido en i se guarda como menor
    intercam elemento en la primera posición*/
    menor = array[i];
    k = i;

    // se busca el segundo elemento más pequeño del arreglo
    for (j=i+1; j<=n-1; j++) {
      /*si el revisado el menor que el ya destinado menor entonces se reemplaza el elemento
        se intercambia con el elemento de segunda posición*/
      if (array[j] < menor) {
        menor = array[j];
        k = j;
      }
    }
    // se reemplazan por arreglos en dichas posiciones
    array[k] = array[i];
    // el i queda como el menor
    array[i] = menor;
  }
  // termina de correr el tiempo
  clock_t finish = clock();
  imprimir_tiempo(start, finish, "Selection"); // imprime el tiempo
}

// reduce el quicksort, es el inicio del método de ordenamiento
void Ordenar::reducir_quicksort(int inicio, int fin, int &pos, int array[])
{
  // variables
  int izq, der, aux;
  bool band;

  // asignación
  izq = inicio;
  der = fin;
  pos = inicio;
  band = true;

  // mientras los índices sean distintos
  while (band) {
    // mientras siga un orden el indice por la derecha disminuye
    while((array[pos] <= array[der]) && (pos!=der)){
      der--;
    }

    // si se iguala la pos con el de la der
    if (pos == der) {
      band = false;
    // o está desordenado (mayor al de la derecha, orden creciente)
    }else{
      // temporal para intercambiar posiciones
      aux = array[pos];
      array[pos] = array[der];
      array[der] = aux;
      pos = der;

      // se evalua ahora por la izquierda por si hay mayores que alteren el orden creciente, izq aumenta
      while ((array[pos] >= array[izq]) && (pos != izq)){
        izq++;
      }

      // si este se iguala con el de la izq
      if (pos == izq) {
        band = false;
      // o está desordenado
      } else {
        // temporal para intercambiar posiciones
        aux = array[pos];
        array[pos] = array[izq];
        array[izq] = aux;
        pos = izq;
      }
    }
  }
}

// método de ordenamiento quicksort O(n x logn)
void Ordenar::quicksort(int array[], int n)
{
  // empieza a correr el tiempo
  clock_t start = clock();
  // variables
  int tope, inicio, fin, pos;
  int pilamenor[n];
  int pilamayor[n];

  // definición de topes
  tope = 0;
  pilamenor[tope] = 0;
  pilamayor[tope] = n - 1;

  /*Mientras que el elemento esté ordenado continua disminuyendo el índice*/
  while (tope >= 0){
    inicio = pilamenor[tope];
    fin = pilamayor[tope];
    tope--;
    reducir_quicksort(inicio, fin, pos, array);

    // ordena los menores
    if(inicio < (pos - 1)){
      tope++;
      pilamenor[tope] = inicio;
      pilamayor[tope] = pos - 1;
    }

    // ordena los mayores
    if (fin > (pos + 1)){
      tope++;
      pilamenor[tope] = pos + 1;
      pilamayor[tope] = fin;
    }
  }
  // termina de correr el tiempo
  clock_t finish = clock();
  imprimir_tiempo(start, finish, "Quicksort"); // imprime el tiempo
}
