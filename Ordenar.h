#include <iostream>
using namespace std;

#ifndef ORDENAR_H
#define ORDENAR_H

class Ordenar {
	public:
		// constructor
		Ordenar();

		// métodos
    void imprimir(int array[], int n, string metodo); // imprime arreglos
		void imprimir_tiempo(clock_t start, clock_t finish, string metodo); // imprime tiempo que demoró un método
		void seleccion(int array[], int n); // método selección
		void reducir_quicksort(int inicio, int fin, int &pos, int array[]); // reduce el metodo quicksort
		void quicksort(int array[], int n); // método quicksort


	private:
		// no hay métodos privados
};
#endif
