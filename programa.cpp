#include <cstring>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sstream>
using namespace std;

#include "Ordenar.h"

void llenar_arreglo(int n, bool ver)
{
  Ordenar ordenar;

  int *array = new int[n];

  srand((unsigned) time(0)); // no repetir números aleatorios

  // se llena el arreglo
  for(int i=0; i<n; i++){
      //números aleatorios y asignación dentro del arreglo
      int random = (rand() % 1000000) + 1;
      array[i] = random;
      if(ver){
        cout << "arreglo[" << i <<  "]=" << array[i] << endl;
      }
  }
  // arreglos para cada método de ordenamiento
  int *array_1 = new int[n];
  int *array_2 = new int[n];

  //asignación
  /*memcpy permite copiar y almacenar la copia en una parte distinta de la memoria
    esto se usa debido a que si solo asignamos el valor y lo mandamos a ordenar entonces
    quedará con el orden del primero. Si cambia primero array_1 entonces cambia array_2
    ya que hace que cambie su origen array
  */
  memcpy(array_1, array, n * sizeof(int)); //sizeof(int) para copiar todo el data
  memcpy(array_2, array, n * sizeof(int));

  // Tabla
  cout << endl;
  cout << "-----------------------------------------" << endl;
  cout << "Método            |Tiempo" << endl;
  cout << "-----------------------------------------" << endl;
  ordenar.seleccion(array_1, n);
  ordenar.quicksort(array_2, n);
  cout << "-----------------------------------------" << endl;

  // si desea ver el contenido
  if(ver){
    ordenar.imprimir(array_1, n, "Selection");
    ordenar.imprimir(array_2, n, "Quicksort");
  }
}

void ordenar(int n)
{
  // por si desea ver o no el contenido de los vectores
  string _ver;
  bool ver;
  cout << "¿Desea ver lo que contienen los vectores? (s para sí): ";
  cin >> _ver;

  // si la respuesta es si
  if(_ver == "S" || _ver == "s"){
    ver = true;
  }else{ // si la respuesta es no, cualquiera que no sea s o S
    ver = false;
  }
  // se llena el arreglo
  llenar_arreglo(n, ver);
}

void ejecutar()
{
  /*parámetro de entrada n
    string para validad si se puede pasar a int
    */
  string n;

  cout << "Inserte cantidad de elementos a ordenar: ";
  cin >> n;
  /*si se logra transformar a int x adopta el valor del int ingresado
    si no se logra convertir porque es otro tipo de variable
    x se transforma en un 0, por lo tanto no entra dentro de los parámetros*/
  std::stringstream geek(n);
  int x = 0;
  geek >> x;

  if(x>0 && x<=1000000){
    // el n ingresado transformado en int como variable x
    ordenar(x);
  }else{
    cout << "Ingresó una cantidad inválida, vuelva a intentarlo." << endl;
    ejecutar();
  }

}

int main(int argc, char *argv[])
{
  ejecutar();
  return 0;
}
